import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  /**
   * Fighter card structure:
   *  1. image
   *  2. name
   *  3. health
   *  4. attack
   *  5. defense
   */
  
  try{
    const img = createFighterImage(fighter);
    const container = createElement({
      tagName: 'div',
      className: 'fighter-info___container', 
    });
    const name = createElement({
      tagName: 'p',
      className: 'fighter-info___title'
    });
    const parameters = createElement({
      tagName: 'ul',
      className: 'fighter-info___details'
    });
    const health = createElement({
      tagName: 'li',
      className: 'fighter-info___detail'
    });
    const attack = createElement({
      tagName: 'li',
      className: 'fighter-info___detail'
    });
    const defense = createElement({
      tagName: 'li',
      className: 'fighter-info___detail'
    });

    name.innerText = fighter.name;
    health.innerText = `Health: ${fighter.health}`;
    attack.innerText = `Attack: ${fighter.attack}`;
    defense.innerText = `Defense: ${fighter.defense}`;

    parameters.append(name, health, attack, defense)
    container.append(parameters);
    fighterElement.append(img, container);
  }catch{
    console.warn(error);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
